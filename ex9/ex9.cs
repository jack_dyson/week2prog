using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ex9
{
    class ex9
    {
        static void Main(string[] args)
        {
            var i = 0;
            var repeat = true;

            do
            {
                Console.WriteLine("Hello, please enter a number: ");
                var a = int.Parse(Console.ReadLine());

                if (repeat)
                {
                    for (i = 0; i < a; i++)
                    {
                        var j = i + 1;
                        Console.WriteLine($"This is Line number {j}");
                    }
                
                }

                if (i == a)
                {
                    Console.WriteLine("Would you like to enter another number? (y/n)");
                    var ans = Console.ReadLine();

                    if ((ans == "y") || (ans == "Y"))
                    {
                        repeat = true;
                    }
                    else
                    {
                        repeat = false;
                        Console.WriteLine("Thank you.");
                    } 
                }
            } while (repeat == true);
        }
    }
}