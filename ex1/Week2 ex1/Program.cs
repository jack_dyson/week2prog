﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Week2_ex1
{
    class Program
    {
        static void Main(string[] args)
        {
            var number1 = 3;
            var number2 = 5;

            if (number1 <= number2)
            {
                Console.WriteLine("Number 1 is smaller than or equal to Number 2.");
            }
            else
            {
                Console.WriteLine("Number 1 is bigger than Number 2.");
            }
        }
    }
}
