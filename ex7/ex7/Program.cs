﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ex7
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Please enter a number: ");

            var i = int.Parse(Console.ReadLine());
            var counter = 20;
            

            do
            {
                var j = i + 1;

                Console.WriteLine($"This is line {j}");
                i++;
            } while (i < counter);
        }
    }
}
