﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ex6
{
    class Program
    {
        static void Main(string[] args)
        {
            var counter = 5;
            var i = 1;

            while (i <= counter)
            {
                Console.WriteLine($"This is line {i}");
                i += 1;
            }
        }
    }
}
