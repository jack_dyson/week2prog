﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ex5
{
    class Program
    {
        static void Main(string[] args)
        {
            var intro = "Welcome. Please answer these true/false questions.";
            var score = 0;
            var ans = true;  
            var q1 = "2 + 3 = 3";
            var q2 = "1 * 3 = 3";
            var q3 = "3 * 4 = 11";
            var q4 = "7 + 2 = 10";
            var q5 = "465321.5 * 742.23 = 345375576";

            Console.WriteLine($"{intro}");

            if (ans)
            {
                Console.WriteLine($"{q1}");
                if (Console.ReadLine() == "false")
                {
                    score += 1;
                    ans = true;
                }
                else
                {
                    Console.WriteLine($"You FAIL with a disappointing score of {score}");
                    ans = false;
                }
            }

            if (ans)
            {
                Console.WriteLine($"{q2}");
                if (Console.ReadLine() == "true")
                {
                    score += 1;
                    ans = true;
                }
                else
                {
                    Console.WriteLine($"You FAIL with a disappointing score of {score}");
                    ans = false;
                }
            }

            if (ans)
            {
                Console.WriteLine($"{q3}");
                if (Console.ReadLine() == "false")
                {
                    score += 1;
                    ans = true;
                }
                else
                {
                    Console.WriteLine($"You FAIL with a disappointing score of {score}");
                    ans = false;
                }
            }

            if (ans)
            {
                Console.WriteLine($"{q4}");
                if (Console.ReadLine() == "false")
                {
                    score += 1;
                    ans = true;
                }
                else
                {
                    Console.WriteLine($"You FAIL with a disappointing score of {score}");
                    ans = false;
                }
            }

            if (ans)
            {
                Console.WriteLine($"{q5}");
                if (Console.ReadLine() == "false")
                {
                    score += 1;
                    ans = true;
                }
                else
                {
                    Console.WriteLine($"You FAIL with a disappointing score of {score}");
                    ans = false;
                }
            }

            if (ans)
            {
                Console.WriteLine($"Well done, Mortal. You may go. {score}/5");
            }
        }
    }
}
