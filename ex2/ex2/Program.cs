﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ex2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Please choose between red or blue: ");
            var colour = Console.ReadLine();

            switch(colour)
            {
                case "red":
                    Console.WriteLine("You chose red - Apples can be red.");
                    break;

                case "blue":
                    Console.WriteLine("You chose blue - The sky is blue.");
                    break;

                default:
                    Console.WriteLine("Pick red or blue, you nonce.");
                    break;
            }
        }
    }
}
