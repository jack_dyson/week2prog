﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ex8
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Bugs are bigger than people.");
            Console.WriteLine("Is this statement true or false? ");
            var ans = bool.Parse(Console.ReadLine());
            var res = "";

            if (ans)
            {
                res = "You are massive idiot. Leave now.";
            }
            else
            {
                res = "Common sense prevails again.";
            }

            Console.WriteLine($"{res}");
        }
    }
}
