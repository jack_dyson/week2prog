﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ex3
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Welcome. Would you like to convert from miles to kms or kms to miles? (kms/miles)");
            var answer = Console.ReadLine();

            switch (answer)
            {
                case "kms":
                    Console.WriteLine("Please enter the amount of kms you'd like to convert to miles ");
                    var a = int.Parse(Console.ReadLine());
                    var b = 0.621371;
                    Console.WriteLine($"{a} kms is {a * b} miles.");
                    break;
                case "miles":
                    Console.WriteLine("Please enter the amount of miles you'd like to convert to kms ");
                    var c = int.Parse(Console.ReadLine());
                    var d = 1.609344;
                    Console.WriteLine($"{c} miles is {c * d} kms.");
                    break;
                default:
                    Console.WriteLine("Please choose one of the options.");
                    break;
            }
        }
    }
}
